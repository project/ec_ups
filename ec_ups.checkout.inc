<?php


/**
 * @file
 * Implementation of checkout API for ecommerce
 *
 *
 */


/**
 * Implementation of hook_checkout_init().
 */
function ec_ups_checkout_init(&$txn) {
  
  
}

/**
 * Implementation of hook_checkout_calculate().
 */
function ec_ups_checkout_calculate(&$form_state) {
  
  
}

/**
 * Implementation of hook_checkout_form().
 */
function ec_ups_checkout_form(&$form, &$form_state) {
  $txn =& $form_state['txn'];
  
  $options = ec_ups_shipping_methods();
  
  $form['ec_ups']['shipping']['shipping_select'] = array(
    '#title' => t('Select Your Shipping Method'),
    '#type' => 'select',
    '#options' => $options,
  );

  $form['ec_ups']['shipping']['shipping_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Select Method'),
      '#access' => count($options) > 1,
#      '#submit' => array('ec_ups_checkout_alter_shipping_method'),
#      '#ahah' => array(
#        'path' => 'checkout/ajax/ec_ups/'. $type,
#        'method' => 'replace',
#      ),
    );
}
/**
 * Implementation of hook_checkout_calculate().
 */
function ec_ups_checkout_validate($form, $form_state) {
  
  
}


/**
 * Implementation of hook_checkout_post_submit().
 */
function ec_ups_checkout_post_update(&$txn) {
  
}  


/**
 * Implementation of hook_checkout_post_submit().
 */
function ec_ups_checkout_post_submit(&$txn) {
  
}  
