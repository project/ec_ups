<?php


/**
 *
 * @file
 * Administration settings for ec_ups
 * 
 * 
 */

function ec_ups_admin_settings() {
 $form= array();
 $form['ups_account'] = array(
    '#title' => t("Account Information"),
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
 );

 $form['ups_account']['ec_ups_license_number'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_license_number', ''),
    '#title' => t('UPS Access License Number'),
 );

 $form['ups_account']['ec_ups_userid'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_userid', ''),
    '#title' => t('UPS User Id'),
 );

 $form['ups_account']['ec_ups_password'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_password', ''),
    '#title' => t('UPS Password'),
 );

  $form['ups_account']['ec_ups_url'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_url', ''),
    '#title' => t('UPS URL for submitting requests'),
 );
 $form['ups_shipper'] = array(
    '#title' => t('Shipper Information'),
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#description' => t('Company Details For this UPS Account.'),
 );
 $form['ups_shipper']['ec_ups_shipper_name'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipper_name', ''),
    '#title' => t('Name'),
 );

 $form['ups_shipper']['ec_ups_shipper_number'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipper_number', ''),
    '#title' => t('Shipper Account Number'),
 );

$form['ups_shipper']['ec_ups_shipper_addr1'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipper_addr1', ''),
    '#title' => t('Address Line 1'),
 );
$form['ups_shipper']['ec_ups_shipper_addr2'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipper_addr2', ''),
    '#title' => t('Address Line 2'),
 );
 $form['ups_shipper']['ec_ups_shipper_addr3'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipper_addr3', ''),
    '#title' => t('Address Line 3'),
 );
 $form['ups_shipper']['ec_ups_shipper_city'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipper_city', ''),
    '#title' => t('City'),
 );

 $form['ups_shipper']['ec_ups_shipper_stateprovince'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipper_stateprovince', ''),
    '#title' => t('State/Province'),
 );
 $form['ups_shipper']['ec_ups_shipper_postalcode'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipper_postalcode', ''),
    '#title' => t('Postal Code/Zip Code'),
 );

  $form['ups_shipfrom'] = array(
    '#title' => t('Ship From Information'),
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#description' => t('Package Origin Address.'),
 );
 $form['ups_shipfrom']['ec_ups_shipfrom_companyname'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipfrom_companyname', ''),
    '#title' => t('Name'),
 );

$form['ups_shipfrom']['ec_ups_shipfrom_addr1'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipfrom_addr1', ''),
    '#title' => t('Address Line 1'),
 );
$form['ups_shipfrom']['ec_ups_shipfrom_addr2'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipfrom_addr2', ''),
    '#title' => t('Address Line 2'),
 );
 $form['ups_shipfrom']['ec_ups_shipfrom_addr3'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipfrom_addr3', ''),
    '#title' => t('Address Line 3'),
 );
 $form['ups_shipfrom']['ec_ups_shipfrom_city'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipfrom_city', ''),
    '#title' => t('City'),
 );
 $form['ups_shipfrom']['ec_ups_shipfrom_stateprovince'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipfrom_stateprovince', ''),
    '#title' => t('State/Province'),
 );
 $form['ups_shipfrom']['ec_ups_shipfrom_postalcode'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ec_ups_shipfrom_postalcode', ''),
    '#title' => t('Postal Code/Zip Code'),
 );

 $form['ups_methods']= array(
    '#title' => t('Shipping Methods'),
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
 );

 $form['ups_methods']['ec_ups_methods_nextdayair_earlyam'] = array(
    '#title' => t('Next Day Air Early AM'),
    '#type' => 'checkbox',
    '#default_value' =>  variable_get('ec_ups_methods_nextdayair_earlyam', 0),
 );
 $form['ups_methods']['ec_ups_methods_nextdayair'] = array(
    '#title' => t('Next Day Air'),
    '#type' => 'checkbox',
    '#default_value' =>  variable_get('ec_ups_methods_nextdayair', 0),
 );
  $form['ups_methods']['ec_ups_methods_nextdayair_saver'] = array(
    '#title' => t('Next Day Air Saver'),
    '#type' => 'checkbox',
    '#default_value' =>  variable_get('ec_ups_methods_nextdayair_saver', 0),
 );
  $form['ups_methods']['ec_ups_methods_2nddayair_am'] = array(
    '#title' => t('2nd Day Air AM'),
    '#type' => 'checkbox',
    '#default_value' =>  variable_get('ec_ups_methods_2nddayair_am', 0),
 );
  $form['ups_methods']['ec_ups_methods_2nddayair'] = array(
    '#title' => t('2nd Day Air'),
    '#type' => 'checkbox',
    '#default_value' =>  variable_get('ec_ups_methods_2nddayair', 0),
 );
 $form['ups_methods']['ec_ups_methods_3dayselect'] = array(
    '#title' => t('3 Day Select'),
    '#type' => 'checkbox',
    '#default_value' =>  variable_get('ec_ups_methods_3dayselect', 0),
 );
 $form['ups_methods']['ec_ups_methods_ground'] = array(
    '#title' => t('Ground'),
    '#type' => 'checkbox',
    '#default_value' =>  variable_get('ec_ups_methods_ground', 0),
 );

 return system_settings_form($form);
}